from django.urls import path
from homepage.views import ajax_with_django_view, getData


app_name = 'homepage'

urlpatterns = [
    path('', ajax_with_django_view, name='django-view'),
    path('getData', getData),
]